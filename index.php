<?php
session_start();

if(isset($_GET['deco'])) {
	session_destroy();
	$_SESSION = null;
}

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>alTheque</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,600,400italic,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="css/flexslider.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- 
	Default Theme Style 
	You can change the style.css (default color purple) to one of these styles
	
	

	-->
	<link rel="stylesheet" href="css/style.css">

	<!-- Styleswitcher ( This style is for demo purposes only, you may delete this anytime. ) -->
	<link rel="stylesheet" id="theme-switch" href="css/style.css">
	<!-- End demo purposes only -->

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>


	<!-- 
		INFO:
		Add 'boxed' class to body element to make the layout as boxed style.
		Example: 
		<body class="boxed">	
	-->
	<body>
	
	<!-- Loader -->
	<div class="fh5co-loader"></div>
	
	<div id="fh5co-page">
		<section id="fh5co-header">
			<div class="container">
				<nav role="navigation">
					<ul class="pull-left left-menu">
						<?php if(isset($_SESSION['access']) && $_SESSION['access'] == 2) { ?>

							<li><a class="btn btn-primary" href="control/administration.php" style="background: #8dc63f; color: #fff; border: 2px solid #8dc63f;">Administration</a></li>

						<?php } else { ?>
							
							<li><a href="about.html">About</a></li>
							<li><a href="tour.html">Tour</a></li>
							<li><a href="pricing.html">Pricing</a></li>

						<?php } ?>

					</ul>

					<h1 id="fh5co-logo"><a href="index.php">altheque<span>.</span></a></h1>
					<ul class="pull-right right-menu">
						<?php
						if(isset($_SESSION['name'])) { ?>
						<li><a href="control/account.php">Mon compte</a></li>
						<li><a href="?deco=1">Déconnexion</a></li>
						<?php } else { ?>
						<li><a href="control/login.php">Se connecter</a></li>
						<li class="fh5co-cta-btn"><a href="control/signup.php">S'inscrire</a></li>
						<?php } ?>
					</ul>
				</nav>
			</div>
		</section>
		<!-- #fh5co-header -->

		<section id="fh5co-hero" class="js-fullheight" style="background-image: url(images/thequeHome.jpg);" data-next="yes">
			<div class="fh5co-overlay"></div>
			<div class="container">
				<div class="fh5co-intro js-fullheight">
					<div class="fh5co-intro-text">
						<!-- 
							INFO:
							Change the class to 'fh5co-right-position' or 'fh5co-center-position' to change the layout position
							Example:
							<div class="fh5co-right-position">
						-->
						<?php
						if(isset($_SESSION['message'])) { 
							echo '<div class="alert alert-success" role="alert">' . $_SESSION['message'] . '</div>'; 
							unset($_SESSION['message']); 
						}
						?>

						<div class="fh5co-left-position">
							<h2 class="animate-box">Read to still learn</h2>
							<a href="control/manageBook.php" target="_blank" class="btn btn-primary">Visit our Shop</a></p>
						</div>
					</div>
				</div>
			</div>
			<div class="fh5co-learn-more animate-box">
				<a href="#" class="scroll-btn">
					<span class="arrow"><i class="icon-chevron-down"></i></span>
				</a>
			</div>
		</section>
		<!-- END #fh5co-hero -->


		<section id="fh5co-projects">
			<div class="container">
				<div class="row row-bottom-padded-md">
					<div class="col-md-6 col-md-offset-3 text-center">
						<h2 class="fh5co-lead animate-box">Our Products</h2>
						<p class="fh5co-sub-lead animate-box">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					</div>
				</div>
				<div class="row">
					
					<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
						<a href="images/img_1.jpg" class="fh5co-project-item image-popup">
							<img src="images/img_1.jpg" alt="Image" class="img-responsive">
							<div class="fh5co-text">
								<h2>Beautiful Sunrise</h2>
								<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							</div>
						</a>
					</div>

					<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
						<a href="images/img_2.jpg" class="fh5co-project-item image-popup">
							<img src="images/img_2.jpg" alt="Image" class="img-responsive">
							<div class="fh5co-text">
								<h2>Cute Little Dog</h2>
								<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							</div>
						</a>
					</div>

					<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
						<a href="images/img_3.jpg" class="fh5co-project-item image-popup">
							<img src="images/img_3.jpg" alt="Image" class="img-responsive">
							<div class="fh5co-text">
								<h2>A Wooden Bridge</h2>
								<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							</div>
						</a>
					</div>

					<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
						<a href="images/img_4.jpg" class="fh5co-project-item image-popup">
							<img src="images/img_4.jpg" alt="Image" class="img-responsive">
							<div class="fh5co-text">
								<h2>Puppy & I in the Farm</h2>
								<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							</div>
						</a>
					</div>

					<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
						<a href="images/img_5.jpg" class="fh5co-project-item image-popup">
							<img src="images/img_5.jpg" alt="Image" class="img-responsive">
							<div class="fh5co-text">
								<h2>A Big Wave of the Blue Sea</h2>
								<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							</div>
						</a>
					</div>

					<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
						<a href="images/img_6.jpg" class="fh5co-project-item image-popup">
							<img src="images/img_6.jpg" alt="Image" class="img-responsive">
							<div class="fh5co-text">
								<h2>Foggy Pine Trees</h2>
								<p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
							</div>
						</a>
					</div>
					
					
				</div>
			</div>
		</section>
		<!-- END #fh5co-projects -->	

		<!-- END #fh5co-features -->

		<!-- END #fh5co-features-2 -->
		
		<section id="fh5co-testimonials">
			<div class="container">
				<div class="row row-bottom-padded-sm">
					<div class="col-md-6 col-md-offset-3 text-center">
						<div class="fh5co-label animate-box">Citations</div>
						<h2 class="fh5co-lead animate-box">You have to read and here's why.</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center animate-box">
						<div class="flexslider">
					  		<ul class="slides">
							   <li>
							      <blockquote>
							      	<p>&ldquo;Se méfier de la littérature. Il faut tout écrire au courant de la plume sans chercher les mots.&rdquo;</p>
							      	<p><cite>&mdash; Jean-Paul Sartre</cite></p>
							      </blockquote>
							   </li>
							   <li>
							    	<blockquote>
							      	<p>&ldquo;Lire, c'est boire et manger. L'esprit qui ne lit pas maigrit comme le corps qui ne mange pas.&rdquo;</p>
							      	<p><cite>&mdash; Victor Hugo</cite></p>
							      </blockquote>
							   </li>
							   <li>
							    	<blockquote>
							      	<p>&ldquo;La lecture d'un bon gros roman est à mains égards comparable à une longue liaison apaisante.&rdquo;</p>
							      	<p><cite>&mdash; Stephen King</cite></p>
							      </blockquote>
							   </li>
							</ul>
						</div>
						<div class="flexslider-controls">
						   <ol class="flex-control-nav">
						      <li class="animate-box"><img src="images/sartre.jpg" alt="Sartre"></li>
						      <li class="animate-box"><img src="images/hugo.jpg" alt="Hugo"></li>
						      <li class="animate-box"><img src="images/king.jpg" alt="King"></li>
						   </ol>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- END #fh5co-testimonials -->

		<section id="fh5co-subscribe">
			<div class="container">
		
				<h3 class="animate-box"><label for="email">Subscribe to our newsletter</label></h3>
				<form action="#" method="post" class="animate-box">
					<i class="fh5co-icon icon-paper-plane"></i>
					<input type="email" class="form-control" placeholder="Enter your email" id="email" name="email">
					<input type="submit" value="Send" class="btn btn-primary">
				</form>

			</div>
		</section>
		<!-- END #fh5co-subscribe -->

		<footer id="fh5co-footer">
			<div class="container">
				<div class="row row-bottom-padded-md">
					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="fh5co-footer-widget">
							<h3>Company</h3>
							<ul class="fh5co-links">
								<li><a href="#">About Us</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Feature Tour</a></li>
								<li><a href="#">Pricing</a></li>
								<li><a href="#">Team</a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="fh5co-footer-widget">
							<h3>Support</h3>
							<ul class="fh5co-links">
								<li><a href="#">Knowledge Base</a></li>
								<li><a href="#">24/7 Call Support</a></li>
								<li><a href="#">Video Demos</a></li>
								<li><a href="#">Terms of Use</a></li>
								<li><a href="#">Privacy Policy</a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="fh5co-footer-widget">
							<h3>Contact Us</h3>
							<p>
								<a href="mailto:contact@altheque.com">contact@altheque.com</a> <br>
								12 rue Alexandre Parodi, <br>
								75000 Paris<br>
								<a href="tel:0235XXXXXX">02 35 XX XX XX</a>
							</p>
						</div>
					</div>

					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="fh5co-footer-widget">
							<h3>Follow Us</h3>
							<ul class="fh5co-social">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-google-plus"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-youtube-play"></i></a></li>
							</ul>
						</div>
					</div>

				</div>
				
			</div>
			<div class="fh5co-copyright animate-box">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<p class="fh5co-left"><small>&copy; 2016 <a href="index.html">Guide</a> free html5. All Rights Reserved.</small></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- END #fh5co-footer -->
	</div>
	<!-- END #fh5co-page -->
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>


	<!-- Main JS (Do not remove) -->
	<script src="js/main.js"></script>

	</body>
</html>

