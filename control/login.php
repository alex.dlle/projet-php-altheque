<?php session_start(); ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inscription</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

    <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
        <a class="navbar-brand" href="../index.php">Return</a>
    </nav>

    <?php

    if(isset($_SESSION['error'])) {
        $arr = $_SESSION['error'];
        echo "<div class='container mt-5 alert alert-danger' role='alert'>" . $arr . "</div>";
        unset($_SESSION['error']);    
    }

    ?>

    <div class="container mt-5 mb-5">
        <form action="login2.php" method="POST">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="signUpEmail">Email</label>
                    <input type="email" class="form-control" id="signUpEmail" name="email" placeholder="Email">
                </div>
                <div class="form-group col-md-6">
                    <label for="signUpPassword">Password</label>
                    <input type="password" class="form-control" id="signUpPassword" name="password" placeholder="Password">
                </div>
            </div>
            
            <input type="submit" class="btn btn-primary" value="Log in"></input>
        </form>
    </div>
</body>

</html>