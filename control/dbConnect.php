<?php

/**
 * classe de connexion à la base de données.
 */
class DbConnect
{
	private $serveur = 'localhost';
    private $login = 'aDessolle';
    private $mot_de_passe = 'aDessolle';
    private $base_de_donnees = 'aDessolle';
    private $pdo;
    
    public function __construct() {

        try {
            $this->pdo = new PDO('mysql:host='.$this->serveur.';dbname='.$this->base_de_donnees, $this->login, $this->mot_de_passe);
        } 
        catch(PDOException $error) {
            echo $error->getCode().' '.$error->getMessage();
        }
    }

    public function getPdo() {
    	return $this->pdo;
    }
}