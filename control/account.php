<?php

include 'dbConnect.php';

session_start();

/**
 * 
 */
class Account
{
	
	function booking($id)
	{
		$pdo = new DbConnect();
		$pdo = $pdo->getPdo();
        $request = $pdo->prepare("SELECT bk.id, b.title, DATE_FORMAT(bk.date, '%d/%m/%Y') as dateResa, DATE_FORMAT(bk.date + INTERVAL 15 DAY, '%d/%m/%Y') AS dateRetour from books b inner join booking bk ON b.id = bk.bookId inner join members m on bk.memberId = m.id WHERE bk.memberId = :id ORDER BY bk.id ASC");
        $request->bindParam(':id', $id);
        $request->execute();
        return $request;
	}
}

$acc = new Account();
$books = $acc->booking($_SESSION['id']);

?>

<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

	<nav class="navbar navbar-light">
        <a class="navbar-brand" href="../index.php">Home</a>
    </nav>

<div class="jumbotron jumbotron-fluid mt-4">
  	<div class="container">
  		<h1 class="display-6">Your profile</h1>
	<div class="card bg-light mx-auto col-md-4">
	  <div class="card-body">
	    <label for="card-name">Email address</label>
	    <input  type="text "class="form-control" id="card-name" placeholder="<?= $_SESSION['mail'] ?>" readonly></input>
	    <label for="card-name">Address</label>
	    <input  type="text "class="form-control" id="card-name" placeholder="<?= $_SESSION['address'] ?>" readonly></input>
	    <label for="card-name">Name</label>
	    <input  type="text "class="form-control" id="card-name" placeholder="<?= $_SESSION['name'] ?>" readonly></input>
	    <label for="card-name">Surname</label>
	    <input  type="text "class="form-control" id="card-name" placeholder="<?= $_SESSION['surname'] ?>" readonly></input>
	    <label for="card-name">Mobile</label>
	    <input  type="text "class="form-control" id="card-name" placeholder="<?= '0' . $_SESSION['mobile'] ?>" readonly></input>
	  </div>
	</div>
</div>
</div>

<div class="jumbotron jumbotron-fluid mt-4">
  	<div class="container">
  		<h1 class="display-6">Your reservations</h1>
		<table class="table table-striped">
			<thead>
			    <tr>
			      <th scope="col">N°</th>
			      <th scope="col">Titre</th>
			      <th scope="col">Réservation</th>
			      <th scope="col">Retour</th>
			    </tr>
			  </thead>
			  <tbody>

		<?php foreach($books as $b) {
			
			?>
			<!-- Mes commandes tableau. -->
			  
			    <tr>
			      <th scope="row"><?=$b['id'];?></th>
			      <td><?=$b['title'];?></td>
			      <td><?=$b['dateResa'];?></td>
			      <td><?=$b['dateRetour'];?></td>
			    </tr>
			  
			<?php

		} ?>

			</tbody>
		</table>
	</div>
</div>
</body>









