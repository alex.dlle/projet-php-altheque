<?php

session_start();

include "dbConnect.php";

class AddBook {

     public function add($title, $description, $image) {
          $pdo = new DbConnect();
          $pdo = $pdo->getPdo();
          $request = $pdo->prepare("INSERT INTO books(title, description, image) VALUES(:title, :description, :image)");
          $request->bindParam(':title', $title);
          $request->bindParam(':description', $description);
          $request->bindParam(':image', $image);
          $request->execute();
     }
}

     

if(isset($_FILES['image']) && !empty($_POST['title']) && !empty($_POST['description']))
{ 
     $dossier = 'upload/';
     $fichier = basename($_FILES['image']['name']);
     if(move_uploaded_file($_FILES['image']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
     {
          $image = 'upload/' . $_FILES['image']['name'];
          $title = $_POST['title'];
          $description = $_POST['description'];

          $addBook = new AddBook();
          $addBook->add($title, $description, $image);

          header('Location: ../index.php');
          $_SESSION['message'] = "Ouvrage ajouté avec succès";
          exit();
     }
     else //Sinon (la fonction renvoie FALSE).
     {
          echo 'Echec de l\'upload !';
     }
} else {
	echo 'non';
}

