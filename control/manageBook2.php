<?php

include 'dbConnect.php';

class BookAvailable
{
	
	public function see() {
		$pdo = new DbConnect();
		$pdo = $pdo->getPdo();
        $request = $pdo->query("SELECT id, title, description FROM books");
        return $request;	
	}

	public function delete($id) {
		$pdo = new DbConnect();
		$pdo = $pdo->getPdo();
		$request = $pdo->prepare("DELETE FROM books WHERE id=:id");
		$request->bindParam(':id', $id);
		$request->execute();
	}

}

$login = new BookAvailable();

if(isset($_GET['delete'])) {
	$login->delete($_GET['delete']);
}

$see = $login->see();

?>

<!DOCTYPE html>
<html>
<head>
	<title>gestion</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body class="bg-dark" style="min-height: 100vh;">

	<table class="table table-striped table-dark">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">Title</th>
	      <th scope="col">Description</th>
	      <th scope="col"></th>
	      <th scope="col"></th>
	    </tr>
	  </thead>
	  <tbody>

	  	<?php
	  	foreach ($see as $se) {
	  	?> 
	  	<tr>
	      <th scope="row"><?= $se['id']; ?></th>
	      <td><?= $se['title']; ?></td>
	      <td><?= $se['description']; ?></td>
	      <td><a href="?delete=<?=$se['id']?>" class="btn btn-danger">Delete</a></td>
	      <td></td>
	    </tr> 
	    <?php
	  	}
	  	?>
	  </tbody>
	</table>

</body>
</html>