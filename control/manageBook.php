<?php

include "dbConnect.php";

class Book
{
	
	public function read() {
		$pdo = new DbConnect();
		$pdo = $pdo->getPdo();
        $request = $pdo->query("SELECT id, title, description, image FROM books");
        return $request;	
	}

	public function search($title) {
		$pdo = new DbConnect();
		$pdo = $pdo->getPdo();
        $request = $pdo->prepare("SELECT id, title, description, image FROM books WHERE title like CONCAT(:title, '%')");
        $request->bindParam(':title', $title);
        $request->execute();

        return $request;
	}

}

$login = new Book();

if(!empty($_GET['search'])) {
	$rd = $login->search($_GET['search']);
} elseif(empty($_GET['search']) || !isset($_GET['search'])) {
	$rd = $login->read();
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>test</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/css/uikit.min.css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>

	<nav class="navbar navbar-light bg-light">
	  <a class="navbar-brand" href="../index.php">Menu principal</a>
	  <?php if(isset($_GET['search']) && !empty($_GET['search'])) { ?>
	  <ul class="navbar-nav ml-5 mr-auto mt-2 mt-lg-0">
		  <li class="nav-item active">
	        <a class="nav-link" href="manageBook.php">Réinitialiser la recherche</a>
	      </li>
	  </ul>
	<?php } ?>
	  <form method="GET" action="?search=<?=$_GET['search']?>" class="form-inline">
	    <input class="form-control mr-sm-2" type="text" name="search" placeholder="Search" aria-label="Search">
	    <input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Recherche">
	  </form>
	</nav>

	<?php
	if($rd->rowCount() == 0) { 
		echo "<div class='container'><div class='mt-5 text-center text-muted alert alert-secondary'>Aucun ouvrage ne correspond à votre recherche <a href='manageBook.php' class='badge badge-dark'> Retour au catalogue</a></div></div>";
	} else {
	?>

	<!-- carte pour chaque ouvrage -->
	<div class="uk-margin-medium-left uk-margin-medium-top uk-margin-medium-right">
		<div class="uk-child-width-1-4 uk-grid-match uk-grid" uk-grid>
		<?php

			foreach ($rd as $opt) { ?>
				
				<div>
					<div class="uk-margin-medium-bottom uk-card uk-card-default">
						<div class="uk-card-media-top uk-height-medium">
							<?= "<img class='uk-height-medium uk-position-top-center' alt='' src='" . $opt['image'] . "'uk-img>"; ?>
						</div>
						<div class="uk-card-body">
							<h3 class="uk-card-title"><?= $opt['title']; ?></h3>
							<p><?= substr($opt['description'], 0, 50); ?></p>
							<a href="?booking=<?=$opt['id'];?>">Réserver</a>
						</div>
					</div>
				</div>
		<?php
			}
		}
		?>
    	</div>
	</div>
</body>
</html>
