<?php

session_start();

include "dbConnect.php";

/**
 * classe pour le login, permet la connexion et la récupération des infos de l'utilisateur.
 */
class Login
{
	
	public function Connection($mail) {
		$pdo = new DbConnect();
		$pdo = $pdo->getPdo();

		$request = $pdo->prepare("SELECT count(*) as isMember, id, mail, password, address, name, surname, mobile, accessLevel FROM members where mail = :mail");
        $request->bindParam(':mail', $mail);
        $request->execute();
        
        return $request;
		
	}

}

$login = new Login();
$co = $login->Connection($_POST['email']);

foreach ($co as $opt) {
	if(isset($_POST['email']) && isset($_POST['password'])) {
	    if($_POST['email'] == $opt['mail'] && password_verify($_POST['password'], $opt['password'])) {
	    	if($opt['isMember'] != 0) {

	    		$_SESSION['id'] = $opt['id'];
	    		$_SESSION['mail'] = $opt['mail'];
	    		$_SESSION['address'] = $opt['address'];
	    		$_SESSION['name'] = $opt['name'];
	    		$_SESSION['surname'] = $opt['surname'];
	    		$_SESSION['mobile'] = $opt['mobile'];
	    		$_SESSION['access'] = $opt['accessLevel'];
	    		header("Location: ../index.php");
	    		exit();

	    	}

		} elseif($opt['isMember'] != 0 && password_verify($_POST['password'], $opt['password']) == false) {
			$_SESSION['error'] = "Mot de passe incorrect";
	    	header("Location: login.php");
		} elseif($opt['isMember'] == 0) {
			$_SESSION['error'] = "Cet identifiant n'existe pas";
	    	header("Location: login.php");
		}
	}
}