<?php

session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<title>administration</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/css/uikit.min.css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/js/uikit.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/js/uikit-icons.min.js"></script>
</head>
<body class="bg-light" style="min-height: 100vh;">
	<?php if($_SESSION['access'] == 2) { ?>

	<!-- carte du choix d'action de l'administrateur -->
	<div class="uk-card uk-card-default uk-width-1-2@m uk-position-center shadow-lg rounded">
	    <div class="uk-card-header">
	        <div class="uk-grid-small uk-flex-middle" uk-grid>
	            <div class="uk-width-expand">
	                <h3 class="uk-card-title uk-margin-remove-bottom">Bienvenue, <?=$_SESSION['name']?></h3>	               
	            </div>
	        </div>
	    </div>
	    <div class="uk-card-body">
	        <p>Pour ajouter un ouvrage, veuillez remplir les champs ci-dessous</p>
	        <form method="POST" action="addBook2.php" enctype="multipart/form-data">

			    <div>
				    <input class="uk-input" type="text" name="title" placeholder="Titre">
				    <div class="uk-margin" uk-margin>
				    <textarea name="description" rows=8 cols=40 class="uk-input uk-width-1-1" placeholder="Description" style="height: 150px;"></textarea>
					</div>
				    
			        <div uk-form-custom="target: true">
			            <input type="file"  accept="image/*" name="image">
			            <input class="uk-input uk-form-width-1-1" type="text" placeholder="Couverture du livre" disabled>
			        </div>

				</div>

			    </div>
			    <div class="uk-card-footer">
			        <a href="adminManage.php" class="uk-button uk-button-default uk-align-left">Retour</a>
			        <input type="submit" class="uk-button uk-button-default uk-align-right" value="Valider">
			    </div>
			</form>
	</div>

	<?php } ?>
	
</body>
</html>