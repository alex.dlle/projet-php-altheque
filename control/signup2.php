<?php

session_start();

include "dbConnect.php";

class Signup {
        
    public function Register($mail, $password, $address, $name, $surname, $mobile) {
        $pdo = new DbConnect();
        $pdo = $pdo->getPdo();
        $request = $pdo->prepare("INSERT INTO members(mail, password, address, name, surname, mobile, accessLevel) VALUES(:mail, :password, :address, :name, :surname, :mobile, 1)");
        $request->bindParam(':mail', $mail);
        $request->bindParam(':password', $password);
        $request->bindParam(':address', $address);
        $request->bindParam(':name', $name);
        $request->bindParam(':surname', $surname);
        $request->bindParam(':mobile', $mobile);
        $request->execute();
    }

    public function Existence($mail) {
        $pdo = new DbConnect();
        $pdo = $pdo->getPdo();
        $request = $pdo->prepare("SELECT count(*) as isRegister FROM members WHERE mail = :mail");
        $request->bindParam(':mail', $mail);
        $request->execute();

        return $request;
    }


}
    

if(!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['address']) && !empty($_POST['name']) && !empty($_POST['surname']) && !empty($_POST['mobile'])) {
        $email = htmlspecialchars($_POST['email']);
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $address = htmlspecialchars($_POST['address']);
        $name = htmlspecialchars($_POST['name']);
        $surname = htmlspecialchars($_POST['surname']);
        $mobile = htmlspecialchars($_POST['mobile']);

        $signup = new Signup();
        $existence = $signup->Existence($email);
        $try = $existence->fetch();

        if($try['isRegister'] == 1) {

            $_SESSION['error'] = "Cet utilisateur existe déja";
            header('Location: signup.php');
            exit();

        } else {

            $signup->Register($email, $password, $address, $name, $surname, $mobile);
            $_SESSION['message'] = "Merci de votre inscription !";
            header('Location: ../index.php');
            exit();

        }

        

} else {
    $_SESSION['error'] = 'veuillez remplir tous les champs';

    header('Location: http://localhost:8888/biblio/guide/control/signup.php');
    exit();
}
