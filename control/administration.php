<?php

session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<title>administration</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.4/css/uikit.min.css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body class="bg-light" style="min-height: 100vh;">

	<?php if($_SESSION['access'] == 2) { ?>
	
	<!-- carte de gestion de modification -->
	<div class="uk-card uk-card-default uk-width-1-2@m uk-position-center shadow-lg rounded">
	    <div class="uk-card-header">
	        <div class="uk-grid-small uk-flex-middle" uk-grid>
	            <div class="uk-width-expand">
	                <h3 class="uk-card-title uk-margin-remove-bottom">Bienvenue, <?=$_SESSION['name']?></h3>	               
	            </div>
	        </div>
	    </div>
	    <div class="uk-card-body">
	        <p>Que voulez vous faire ?</p>
	        <ul class="uk-list">
			<li><a class="uk-button uk-button-secondary" href="">Gestion des réservations</a></li>
			<li><a class="uk-button uk-button-secondary" href="adminManage.php">Gestion des ouvrages en ligne</a></li>
			</ul>
	    </div>
	    <div class="uk-card-footer">
	        <a href="../index.php" class="uk-button uk-button-text">Retour à l'accueil</a>
	    </div>
	</div>

	<?php } else { header('Location: ../index.php'); }?>
	
</body>
</html>